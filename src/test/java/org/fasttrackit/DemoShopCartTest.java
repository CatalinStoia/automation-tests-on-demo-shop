package org.fasttrackit;

import io.qameta.allure.*;
import jdk.jfr.Description;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("User can add products to cart and navigate from and to cart page")
public class DemoShopCartTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }

    @Test
    @Description("User can access cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DEMO-001")
    @Story("User can navigate to cart page")
    public void user_can_navigate_to_cart_page() {
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart page");
    }

   @Test
   @Description("User can navigate to homepage from cart page.")
   @Severity(SeverityLevel.NORMAL)
   @Owner("Stoia Catalin")
   @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
   @Issue("DEMO-002")
   @Story("User can navigate from cart page to homepage")
    public void user_can_navigate_to_homepage_from_cart_page() {
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Products page");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Description("User can add products to cart by clicking the product cards.")
    @Severity(SeverityLevel.BLOCKER)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DEMO-003")
    @Story("User can add a product to cart")
    public void user_can_add_product_to_cart_from_product_cards(ProductData productData) {
        Product product = new Product(productData.getId());
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Description("User can add two products to cart by clicking the product cards.")
    @Severity(SeverityLevel.BLOCKER)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DEMO-004")
    @Story("User can add products to cart")
    public void user_can_add_two_products_to_cart_from_product_cards(ProductData productData) {
        Product product = new Product(productData.getId());
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two products to cart, badge shows 2.");
    }
}
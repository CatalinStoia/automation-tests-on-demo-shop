package org.fasttrackit;

import io.qameta.allure.*;
import jdk.jfr.Description;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
@Feature("Cart management")

public class CartManagementTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
        header.clickOnTheShoppingBagIcon();
    }

    @Test
    @Description("User navigates to empty cart page and gets message: How about adding some products in your cart?")
    @Severity(SeverityLevel.MINOR)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("CART-001")
    public void when_user_navigates_to_cart_page_empty_cart_page_message_is_displayed() {
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Description("User ads one product to cart and does not get the message: How about adding some products in your cart?")
    @Severity(SeverityLevel.MINOR)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("CART-002")
    public void adding_one_product_to_cart_empty_cart_message_is_not_shown(ProductData productData) {
        Product product = new Product(productData.getId());
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Description("User can add more products to cart page from within the cart page.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("CART-003")
    public void user_can_increment_the_amount_of_a_product_in_cart_page(ProductData productData) {
        Product product = new Product(productData.getId());
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        item.increaseAmount();
        assertEquals(item.getItemAmount(), "2");
    }

    @Test
    @Description("User can remove products from within the cart page.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("CART-004")
    public void user_can_reduce_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.reduceAmount();
        assertEquals(item.getItemAmount(), "1");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Description("User can remove product from cart")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("CART-005")
    public void user_can_remove_product_from_cart(ProductData productData) {
        Product product = new Product(productData.getId());
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheTrashIcon();
        assertFalse(header.isShoppingBadgeVisible());

    }
}

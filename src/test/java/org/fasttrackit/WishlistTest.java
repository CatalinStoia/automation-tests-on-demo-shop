package org.fasttrackit;

import io.qameta.allure.*;
import jdk.jfr.Description;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class WishlistTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }

    @Test
    @Description("User can access Wishlist Page.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("WISH-001")
    public void user_can_navigate_to_Wishlist_Page() {
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on the Wishlist page.");
    }

    @Test
    @Description("User can navigate to home page from wishlist page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("WISH-002")
    public void user_can_navigate_to_Home_Page_from_Wishlist_Page() {
        header.clickOnTheWishlistIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page.");
    }

    @Test
    @Description("User can navigate to cart page from wishlist page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("WISH-003")
    public void user_can_navigate_to_Cart_Page_from_Wishlist_Page(){
        header.clickOnTheWishlistIcon();
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart page.");
    }

    @Test
    @Description("User can add a product to wishlist page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("WISH-004")
    public void user_can_add_one_product_to_Wishlist_page() {
        Product product = new Product("5");
        product.clickOnTheProductWishlistButton();
        assertTrue(header.isWishlistPageBadgeVisible());
        String badgeValue = header.getWishlistCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to wishlist, badge must show 1." );
    }

    @Test
    @Description("User can add products to wishlist page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("WISH-005")
    public void user_can_add_two_products_to_Wishlist_page() {
        Product product = new Product("5");
        product.clickOnTheProductWishlistButton();
        Product product1 = new Product("6");
        product1.clickOnTheProductWishlistButton();
        assertTrue(header.isWishlistPageBadgeVisible());
        String badgeValue = header.getWishlistCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding one product to wishlist, badge must show 2." );
        }

}

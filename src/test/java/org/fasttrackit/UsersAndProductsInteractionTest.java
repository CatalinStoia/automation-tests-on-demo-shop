package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import jdk.jfr.Description;
import org.fasttrackit.dataprovider.UserAndProductsDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertFalse;

public class UsersAndProductsInteractionTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();
    CartPage cartPage = new CartPage();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickOnTheResetButton();
    }

    @Test(dataProviderClass = UserAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Description("Logged in user can add a product to cart")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("UAP-001")
    public void logged_in_user_can_add_product_to_cart(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product = new Product(productData.getId());
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());

   }

}

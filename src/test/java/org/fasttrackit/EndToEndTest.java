package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.fasttrackit.dataprovider.UserAndProductsDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
@Epic("User journey trough Demo Shop")

public class EndToEndTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();
    CartPage cartPage = new CartPage();
    Checkout checkout = new Checkout();
    OrderSummary orderSummary = new OrderSummary();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }

    @Test(dataProvider = "userAndProduct", dataProviderClass = UserAndProductsDataProvider.class)
    @Description("User journey trough Demo Shop from login until checkout")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("END-001")
    public void user_adds_product_to_cart_then_completes_order(ValidAccount account, ProductData productData){
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product = new Product(productData.getId());
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutIcon();
        checkout.typeInFirstName("Catalin");
        checkout.typeInLastName("Stoia");
        checkout.typeAddress("Floresti, Cluj");
        checkout.clickOnTheContinueCheckoutButton();
        orderSummary.clickOnCompleteOrder();
        assertEquals(orderSummary.getOrderCompleteMessage(), "Order complete", "Order complete message");

    }
}

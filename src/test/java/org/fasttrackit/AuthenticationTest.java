package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import jdk.jfr.Description;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
@Feature("Login")
public class AuthenticationTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickOnTheResetButton();
    }


    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Description("Valid user can log in with valid credentials.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("AUTH-001")
    @Story("Authentication with valid credentials")
    public void valid_user_can_login_with_valid_credentials(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Logged in with valid user, expected greetings message to be:" + account.getGreetingsMsg());
    }

    @Test(dataProvider = "invalidCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Description("User cannot login when invalid credentials are used for login.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("AUTH-002")
    @Story("Login failed when invalid credentials are used")
    public void non_valid_logins_with_invalid_credentials(InvalidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        String errorMsg = modal.getErrorMsg();
        assertTrue(errorMessageVisible, "Error message is shown when invalid login credentials are used for login.");
        assertEquals(errorMsg, account.getErrorMsg());

    }

    @Test (dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Description("Valid user can log off.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Stoia Catalin")
    @Lead("Stoia Catalin")
    @Link(name = "FasttrackIT Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("AUTH-004")
    @Story("Valid user can log off")
    public void valid_user_can_log_off(ValidAccount account){
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Logged off, expected message to be Hello guest!");
    }

}


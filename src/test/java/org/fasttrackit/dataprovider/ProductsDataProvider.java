package org.fasttrackit.dataprovider;

import org.fasttrackit.ProductData;
import org.fasttrackit.ValidAccount;
import org.testng.annotations.DataProvider;

public class ProductsDataProvider {
  static ProductData agc = new ProductData("1", "Awesome Granite Chips", "$15.99");
  static ProductData ich = new ProductData("2", "Incredible Concrete Hat", "$7.99");

    public ProductData getAgc() {
        return agc;
    }

    public ProductData getIch() {
        return ich;
    }

    @DataProvider(name = "products")
    public Object[][] getCredentials() {

        return new Object[][]{
                {agc},
                {ich},
        };
    }
}

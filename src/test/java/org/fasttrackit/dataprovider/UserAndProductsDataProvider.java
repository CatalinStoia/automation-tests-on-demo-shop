package org.fasttrackit.dataprovider;

import org.testng.annotations.DataProvider;

import static org.fasttrackit.dataprovider.ProductsDataProvider.agc;
import static org.fasttrackit.dataprovider.ProductsDataProvider.ich;


public class UserAndProductsDataProvider {
    @DataProvider(name = "userAndProduct")
    public Object[][] createData1() {
        return new Object[][]{
                {AuthenticationDataProvider.beetle, agc},
                {AuthenticationDataProvider.beetle, ich},
                {AuthenticationDataProvider.turtle, agc},
                {AuthenticationDataProvider.turtle, ich},
                {AuthenticationDataProvider.dino, agc},
                {AuthenticationDataProvider.dino, ich},

        };
    }

}

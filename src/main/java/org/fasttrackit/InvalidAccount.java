package org.fasttrackit;

public class InvalidAccount extends Account {
    public String getErrorMsg() {
        return errorMsg;
    }

    private final String errorMsg;

    public InvalidAccount(String user, String password, String errorMsg) {
        super(user, password);
        this.errorMsg = errorMsg;
    }
}

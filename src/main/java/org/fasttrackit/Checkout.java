package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Checkout {
    private final SelenideElement firstName = $("#first-name");
    private final SelenideElement lastName = $("#last-name");
    private final SelenideElement addressInfo = $("#address");
    private final SelenideElement continueCheckout = $(".fa-angle-right");
@Step
    public void typeInFirstName(String firstNameInfo) {
        System.out.println("Click on the first name field.");
        firstName.click();
        System.out.println("Type in " + firstNameInfo);
        firstName.type(firstNameInfo);
    }
    @Step
    public void typeInLastName (String lastNameInfo) {
        System.out.println("Click on the last name field.");
        lastName.click();
        System.out.println("Type in " + lastNameInfo);
        lastName.type(lastNameInfo);
    }
    @Step
    public void typeAddress (String addressFieldInfo) {
        System.out.println("Click on the address field.");
        addressInfo.click();
        System.out.println("Type in " + addressFieldInfo);
        addressInfo.type(addressFieldInfo);
    }
    @Step
    public void clickOnTheContinueCheckoutButton() {
        continueCheckout.click();
    }
}

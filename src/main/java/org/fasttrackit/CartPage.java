package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private final SelenideElement emptyCartPageElement = $(".text-center");
    private final SelenideElement trashIcon = $(".fa-trash");
    private final SelenideElement checkoutIcon = $(".fa-angle-right");

    public CartPage() {
    }
    @Step
    public void clickOnTheTrashIcon() {
        trashIcon.click();
        System.out.println("Clicking on the trash icon.");
    }
    @Step
    public void clickOnTheCheckoutIcon() {
        checkoutIcon.click();
        System.out.println("Clicking on the checkout icon.");
    }

    public String getEmptyCartPageText() {
        return this.emptyCartPageElement.text();
    }

    public boolean isEmptyCartMessageDisplayed() {
        return emptyCartPageElement.isDisplayed();
    }
}

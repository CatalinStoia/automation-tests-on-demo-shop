package org.fasttrackit;

import io.qameta.allure.Step;

public class ValidAccount extends Account {

    private final String greetingsMsg;

    public ValidAccount(String user, String password) {
        super(user, password);
        this.greetingsMsg = "Hi " + user + "!";
    }
@Step
    public String getGreetingsMsg() {
        return greetingsMsg;
    }
}

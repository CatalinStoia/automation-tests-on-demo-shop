package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement logoutButton = $(".navbar .fa-sign-out-alt");
    private final SelenideElement greetingsElement = $(".navbar-text span span");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges = $$(".shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");
    private final SelenideElement wishlistPageBadge = $(".shopping_cart_badge");
private final ElementsCollection wishlistPageBadges = $$(".shopping_cart_badge");
    @Step("Click on the Login Button.")
    public void clickOnTheLoginButton() {
        loginButton.click();
        System.out.println("Click on the Login button.");
    }
    @Step("Click on the logout button.")
    public void clickOnTheLogoutButton() {
        logoutButton.click();
        System.out.println("Click on the logout button.");
    }

    public String getGreetingsMessage() {
        return greetingsElement.text();
    }
    @Step
    public void clickOnTheWishlistIcon() {
        System.out.println("Click on the Wishlist button.");
        wishlistButton.click();
    }
    @Step
    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping bag icon.");
        homePageButton.click();
    }
    @Step
    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart Icon");
        cartIcon.click();
    }

    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }

    public boolean isShoppingBadgeVisible() {
        return !this.shoppingCartBadges.isEmpty();
    }
    public String getWishlistCartBadgeValue() {
    return this.wishlistPageBadge.text();
    }
    public boolean isWishlistPageBadgeVisible() {
    return !this.wishlistPageBadges.isEmpty();
    }
}

package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement reset = $(".fa-undo");
@Step("Reset App state")
    public void clickOnTheResetButton() {
        this.reset.click();
    }
}

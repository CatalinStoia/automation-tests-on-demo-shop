package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class OrderSummary {
    public final SelenideElement completeOrder =  $(".fa-angle-right");
    public final SelenideElement orderComplete = $(".text-muted");
    public void clickOnCompleteOrder(){
        completeOrder.click();
    }
    public String getOrderCompleteMessage() {return orderComplete.text();}
}




## **_Automation Tests on Demo Shop_**

Introduction into Automation Testing by
Catalin Andrei Stoia



#### Link: https://fasttrackit-test.netlify.app/#/

#### Repo: https://gitlab.com/CatalinStoia/automation-tests-on-demo-shop/-/tree/master


**Demo Shop** este o platforma online structurata sub forma unui webshop.
Platforma permite implementarea si aplicarea tehnologiilor si
principiilor testarii automate folosind diverse scenarii de testare.

In aceasta aplicatie, am creeat scenarii legate de functia de logare, de
cart management, pagina de produse preferate si de sortarea produselor pe pagina.
De asemenea, am reprodus si parcursul unui utilizator pe platforma Demo Shop.

<img src="C:\Users\asus\OneDrive\Desktop\Allure results.PNG"/>

**Tehnologii folosite**:

* Java
* IntelliJ Idea Community Edition
* Apache Maven
* Selenide
* CSS
* Allure
* Data Provider
* Gitlab
* Google Chrome


_Va multumesc pentru interesul acordat!_